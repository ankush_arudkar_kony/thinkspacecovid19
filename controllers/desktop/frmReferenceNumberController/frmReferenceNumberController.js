define({ 

  status: null,
  onNavigate: function(context, isBack){
    if(!isBack){
      this.view.lblRefNumber.text = context["id"]
    }
  },

  showLoader: function(text){
    this.status = document.createElement("div");
    this.status.innerHTML = text;
    this.status.style.display = "block";
    this.status.style.position = "fixed";
    this.status.style.bottom = "20px";
    this.status.style.left = "50%";
    this.status.style.setProperty("transform", "translate(-50%, 0)");
    this.status.style.width = "80%";
    this.status.style.maxWidth = "500px";
    this.status.style.zIndex = "1000000";
    this.status.style.fontFamily = "sans-serif";
    this.status.style.padding = "10px";
    this.status.style.background = "rgb(0,0,0,0.7)"
    this.status.style.boxShadow = "0px 0px 5px #a0a0a0";
    this.status.style.color = "white";
    this.status.style.setProperty("backdrop-filter", "blur(5px) saturate(150%)")
    this.status.style.borderRadius = "10px";
    document.body.appendChild(this.status);
  },

  hideLoader: function(){
    if(this.status)
      this.status.remove();
  },

  
  init: function() {
    this.view.lblRefNumber.onClick = this.copyId;
    this.view.btnGoBack.onClick = this.goToJourneyBegin;
  },

  goToJourneyBegin: function() {
      var nextForm = new kony.mvc.Navigation("frmJourneyBegin");
      nextForm.navigate();
  },
  
  copyId: function(){
    let temp = document.createElement("textarea");
    temp.value = this.view.lblRefNumber.text.replace(/\"/g, "");
    temp.style.top = "0px";
    temp.style.left = "0px";
    temp.style.position = "fixed";
    temp.style.opacity = "0";
    document.body.appendChild(temp);
    temp.focus();
    temp.select();
    try {
      document.execCommand('copy');
      this.showLoader("Refrence Id copied")
      setTimeout(()=>{
        this.hideLoader();
      }, 2000)
    } catch (err) {
      alert("Copy permission denied, please manally select and copy the reference Id")
    }

    temp.remove();
  }

});