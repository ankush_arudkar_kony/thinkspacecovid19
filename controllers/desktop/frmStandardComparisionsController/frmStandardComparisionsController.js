define({ 

  data: {},
  onNavigate: function(context, backNavigate){
    if(!backNavigate){
      this.data = context["data"]
      this.setData(this.data);
      this.calculateCuttoffs();
    }
  },

  setData: function(data){
    this.view.lblRevenueValue.text = "$"+data["revenue"];
    this.view.tbxSupplies.text = data["suppliesExpense"];
    this.view.tbxPayroll.text = data["payrollExpense"];
    this.view.tbxUtility.text = data["utilityBills"];
    this.view.tbxOther.text = data["otherExpense"];

    data["epxense"] = parseFloat(data["suppliesExpense"]) +parseFloat(data["payrollExpense"]) + parseFloat(data["utilityBills"]) + parseFloat(data["otherExpense"]);
  },

  percent: function(value, total) {
    return (parseFloat(value)/total)*100;
  },

  /**
   * 1: red, 2: yellow, 3: green
   */
  setValueSkin: function(wid, num, max){
    if(num > max + 6){
      wid.skin = "lblHeading1Red";
      wid.text = (num - max).toFixed(2) + "% higher";
    }
    else if(num > max){
      wid.skin = "lblHeading1Yellow";
      wid.text = (num - max).toFixed(2) + "% higher";
    }
    else{
      wid.skin = "lblHeading1Green";
      wid.text =  (max - num).toFixed(2) + "% lower";
    }
    this.view.forceLayout();
  },

  setGraph: function(wid, lbl, percent, max, amount){
    if(percent > max + 6){
      wid.skin = "sknFlxRedBar";
    }
    else if(percent > max){
      wid.skin = "sknFlxYellowBar";
    }
    else{
      wid.skin = "sknFlxGreenBar";
    }
    wid.width = `${percent}%`;
    lbl.text = `$${amount.toFixed(2)}`;
    this.view.forceLayout();
  },

  /**
   * expenses: 80%
   * 
   * supplies: 40% 
   * payroll: 30%
   * utilities: 10%
   * other: 20%
   */
  calculateCuttoffs: function(){
    let supply = parseFloat(this.view.tbxSupplies.text)
    let utlility = parseFloat(this.view.tbxUtility.text)
    let payroll = parseFloat(this.view.tbxPayroll.text)
    let other = parseFloat(this.view.tbxOther.text)
    let currentExpense = supply + utlility + payroll + other;
    let expenses = (parseFloat(this.data["revenue"]) + (parseFloat(this.data["epxense"]) - currentExpense)).toFixed(2) * 0.8;
    let supplyEx = this.percent(supply, expenses)
    this.setValueSkin(this.view.lblSupply, supplyEx, 40);
    this.setGraph(this.view.flxSuppliesCurrent, this.view.lblSuppliesCurrent, supplyEx, 40, supply);
    let payrollEx = this.percent(payroll, expenses)
    this.setValueSkin(this.view.lblPayroll, payrollEx, 30);
    this.setGraph(this.view.flxPayrollCurrent, this.view.lblPayrollCurrent, payrollEx, 30, payroll);
    let utilityEx = this.percent(utlility, expenses);
    this.setValueSkin(this.view.lblUtility, utilityEx, 10);
    this.setGraph(this.view.flxUtilityCurrent, this.view.lblUtilityCurrent, utilityEx, 10, utlility);
    let otherEx = this.percent(other, expenses);
    this.setValueSkin(this.view.lblOther, otherEx, 20);
    this.setGraph(this.view.flxOtherCurrent, this.view.lblOtherCurrent,  otherEx, 20, other);
    let newExpense = ((parseFloat(this.data["revenue"]) + (parseFloat(this.data["epxense"]) - currentExpense)).toFixed(2))*0.8
    this.view.lblSuppliesStandard.text = `$${(newExpense*0.4).toFixed(2)}`
    this.view.lblPayrollStandard.text = `$${(newExpense*0.3).toFixed(2)}`
    this.view.lblUtilityStandard.text = `$${(newExpense*0.1).toFixed(2)}`
    this.view.lblOtherStandard.text = `$${(newExpense*0.2).toFixed(2)}`
    this.view.lblRevenueValue.text = "$" + ((parseFloat(this.data["revenue"]) + (parseFloat(this.data["epxense"]) - currentExpense)).toFixed(2));
  },

  init: function() {
    this.view.btnExploreFoodDelivery.cursorType = "pointer";
    this.view.btnExploreFoodDelivery.onClick = this.goToDeliveryOptions;
    this.view.tbxSupplies.onTextChange = this.calculateCuttoffs;
    this.view.tbxUtility.onTextChange = this.calculateCuttoffs;
    this.view.tbxPayroll.onTextChange = this.calculateCuttoffs;
    this.view.tbxOther.onTextChange = this.calculateCuttoffs;
  },

  goToDeliveryOptions: function() {
      var nextForm = new kony.mvc.Navigation("frmDeliveryOptions");
      nextForm.navigate({
        "data": this.data
      });
  }
  
});