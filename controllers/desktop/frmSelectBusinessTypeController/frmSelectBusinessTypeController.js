define({ 

  init: function() {
    this.view.flxRestaurant.onClick = this.goToFormDetails;
  },

  goToFormDetails: function() {
    var nextForm = new kony.mvc.Navigation("frmRestaurantDataForm");
    nextForm.navigate();
  }

});