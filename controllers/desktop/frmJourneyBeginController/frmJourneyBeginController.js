define({

  status: null,
  init: function () {
    this.view.betGetStarted.cursorType = "pointer";
    this.view.btnResume.cursorType = "pointer";
    this.view.betGetStarted.onClick = this.getStarted;
    this.view.btnResume.onClick = this.goToReview;
    let scope = this;
    setTimeout(function () {
      scope.view.forceLayout();
    }, 1000);
  },

  showLoader: function(text){
    this.status = document.createElement("div");
    this.status.innerHTML = text;
    this.status.style.display = "block";
    this.status.style.position = "fixed";
    this.status.style.bottom = "20px";
    this.status.style.left = "50%";
    this.status.style.setProperty("transform", "translate(-50%, 0)");
    this.status.style.width = "80%";
    this.status.style.maxWidth = "500px";
    this.status.style.zIndex = "1000000";
    this.status.style.fontFamily = "sans-serif";
    this.status.style.padding = "10px";
    this.status.style.background = "rgb(0,0,0,0.7)"
    this.status.style.boxShadow = "0px 0px 5px #a0a0a0";
    this.status.style.color = "white";
    this.status.style.setProperty("backdrop-filter", "blur(5px) saturate(150%)")
    this.status.style.borderRadius = "10px";
    document.body.appendChild(this.status);
  },

  hideLoader: function(){
    if(this.status)
      this.status.remove();
  },

  goToReview: function () {
    this.showLoader("Finding your reference number...");
    fetch(`https://cors-anywhere.herokuapp.com/https://salty-island-91094.herokuapp.com/cashflowinator/${this.view.tbxRefNumber.text}`, {
      mode: 'cors',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        "Origin": "https://salty-island-91094.herokuapp.com/cashflowinator/"
      },
    })
      .then(response => {
        let dataF = response.text();
        return dataF;
      })
      .then(dataF => {
        var nextForm = new kony.mvc.Navigation("frmReivewLater");
        nextForm.navigate({
          "refId": this.view.tbxRefNumber.text,
          "data": JSON.parse(dataF)
        });
      this.hideLoader();
      })
      .catch(err => {
        this.hideLoader();
        alert("Incorret Reference ID provided");
      })
  },
    
  getStarted: function () {
    var nextForm = new kony.mvc.Navigation("frmSelectBusinessType");
    nextForm.navigate();
  }

});