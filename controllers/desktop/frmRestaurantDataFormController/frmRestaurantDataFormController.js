define({ 

  init: function() {
    this.view.btnCalculate.cursorType = "pointer";
    this.view.radioHomeDelivery.setEnabled(false);
    this.view.btnCalculate.onClick = this.goToStandardComparisions;
  },

  getData: function() {
    let data = {};
    data["NAICS"] = this.view.tbxNAICScode.text;
    data["city"] = this.view.tbxCity.text;
    data["yearsActive"] = this.view.lstBoxYearsActive.selectedKeyValue[0];
    data["revenue"] = this.view.tbxRevenue.text;
    data["suppliesExpense"] = this.view.tbxSupplyExpenses.text;
    data["payrollExpense"] = this.view.tbxPayroll.text;
    data["utilityBills"] = this.view.tbxUtilityBills.text;
    data["otherExpense"] = this.view.tbxOtherExpenses.text;
    data["homeDelivery"] = this.view.radioHomeDelivery.selectedKeyValue[0];
    data["takeOuts"] = this.view.radioTakeOuts.selectedKeyValue[0];
    data["localSupplies"] = this.view.radioLocalSupplies.selectedKeyValue[0];
    data["desiredRevenue"] = this.view.lblDesiredResults.text;
    return data;
  },

  goToStandardComparisions: function() {
    var nextForm = new kony.mvc.Navigation("frmStandardComparisions");
    nextForm.navigate({
      data: this.getData()
    });
  }

});