define({
  data: {},
  status: null,
  onNavigate: function (context, isBack) {
    if (!isBack) {
      this.data = context["data"]
    }
  },

  showLoader: function (text) {
    this.status = document.createElement("div");
    this.status.innerHTML = text;
    this.status.style.display = "block";
    this.status.style.position = "fixed";
    this.status.style.bottom = "20px";
    this.status.style.left = "50%";
    this.status.style.setProperty("transform", "translate(-50%, 0)");
    this.status.style.width = "80%";
    this.status.style.maxWidth = "500px";
    this.status.style.zIndex = "1000000";
    this.status.style.fontFamily = "sans-serif";
    this.status.style.padding = "10px";
    this.status.style.background = "rgb(0,0,0,0.7)"
    this.status.style.boxShadow = "0px 0px 5px #a0a0a0";
    this.status.style.color = "white";
    this.status.style.setProperty("backdrop-filter", "blur(5px) saturate(150%)")
    this.status.style.borderRadius = "10px";
    document.body.appendChild(this.status);
  },

  hideLoader: function () {
    if (this.status)
      this.status.remove();
  },


  init: function () {
    this.view.btnCalculateCosts.cursorType = "pointer";
    this.view.btnCalculateCosts.onClick = this.calculateCost;
    this.view.btnSave.cursorType = "pointer";
    this.view.btnSave.onClick = this.goToRefernceNum;
  },

  getData: function () {
    let data = [];
    for (let i = 1; i < 6; i++) {
      let temp = {
        "name": this.view[`tbxMeal${i}Name`].text,
        "unitPrice": parseFloat(this.view[`tbxMeal${i}Amount`].text),
        "qty": parseFloat(this.view[`tbxMeal${i}Qty`].text)
      }
      data.push(temp)
    }
    return data;
  },

  calculateCost: function () {
    // service charge in %
    const grubhub = 13;
    const ubereats = 15;
    const doordash = 10;
    let deductPercent = function (val, per) {
      return (val - (val * per) / 100).toFixed(2)
    }
    let total = 0;
    let data = this.getData();
    for (val of data) {
      total += val["unitPrice"] * val["qty"];
    }

    this.view.lblDoordashRevenue.text = "$ " + deductPercent(total, doordash);
    this.view.lblGrubhubRevenue.text = "$" + deductPercent(total, grubhub);
    this.view.lblUberEatsRevenue.text = "$" + deductPercent(total, ubereats);
  },

  goToRefernceNum: function () {
    this.showLoader("Please wait, we are saving your data")
    let dataStr = JSON.stringify(this.data);
    fetch("https://cors-anywhere.herokuapp.com/https://salty-island-91094.herokuapp.com/cashflowinator/", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: dataStr,
    })
      .then(response => {
        this.hideLoader()
        let id = response.text();
        return id;
      })
      .then(id => {
        var nextForm = new kony.mvc.Navigation("frmReferenceNumber");
        nextForm.navigate({
          "id": id
        });
      })
      .catch(err => {
        this.hideLoader()
        alert("Something went wrong, please try later");
      })
  }

});