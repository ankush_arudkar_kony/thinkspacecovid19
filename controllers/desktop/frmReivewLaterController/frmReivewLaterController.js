define({ 

  data: {},
  id: "",
  onNavigate: function(context, isBack){
    if(!isBack){
      this.id = context["refId"];
      this.data = context["data"];
    }
  }, 

  init: function() {
    this.view.btnSubmitData.onClick = this.showResult;
  },

  showResult: function() {
    let curRevenueWeekly = parseFloat(this.view.tbxRevenueUpdate.text);
    let prevRevenueWeekly = (parseFloat(this.data["revenue"])/30)*7;
    if(curRevenueWeekly > prevRevenueWeekly + 100){
      this.view.flxCongrats.setVisibility(true);
      this.view.flxGetGoing.setVisibility(false);
    }
    else{
      this.view.flxCongrats.setVisibility(false);
      this.view.flxGetGoing.setVisibility(true);
    }
    this.view.forceLayout();
    this.data[`${Date.now()}`] = this.view.tbxRevenueUpdate.text;
    fetch(`https://cors-anywhere.herokuapp.com/https://salty-island-91094.herokuapp.com/cashflowinator/${this.id}`, {
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        "Origin": "https://salty-island-91094.herokuapp.com/cashflowinator/"
      },
      method: 'PUT',
      body: JSON.stringify(this.data)
    })
      .then(response => {
        return response.text();
      })
      .catch(err => {
        // alert("Couldnot update data, please try later");
      })
  }

 
  
});